using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using formapplication.Entities;
using formapplication.Enums;

namespace formapplication
{
     public class FormDbContext : DbContext
     {

          public DbSet<Record> Records { get; set; }

          public FormDbContext(DbContextOptions<FormDbContext> options)
               : base(options)
          {
               Database.EnsureCreated();
          }

          // Seeds a context object with some seed data representing user input
          public bool CreateSeedData()
          {
            var context = this;

            // Don't seed if already populated
            if (context.Records.Any()) {
                return true;
            }

            try {
               var records = new List<Record>() {
                    new Record() {
                         Type = Type.Asset.ToString(),
                         Name = "Entry1",
                         Balance = 1000
                    },
                    new Record() {
                         Type = Type.Liability.ToString(),
                         Name = "Entry2",
                         Balance = 500
                    },
                    new Record() {
                         Type = Type.Asset.ToString(),
                         Name = "Entry3",
                         Balance = 1000
                    },
                    new Record() {
                         Type = Type.Liability.ToString(),
                         Name = "Entry4",
                         Balance = 500
                    }
               };

               context.AddRange(records);
               context.SaveChanges();
               return true;
          }
          catch (System.Exception e) {
               System.Console.WriteLine(e.ToString());
               return false;
          }
            
        }

        
     }
}