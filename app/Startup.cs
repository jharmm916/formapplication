using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;

using formapplication.Controllers;

// TODO change to FormApplication 
namespace formapplication
{
    public class Startup
    {

        // Application built using ASP.NET, Entity Framework, and tested on a Docker based SQL Server

        // DEPLOYMENT NOTES:
        // use Homebrew to install mssql-tools to access container: 'brew install msodbcsql mssql-tools'
        // docker command to pull sql server 2019: 'sudo docker pull mcr.microsoft.com/mssql/rhel/server:2019-RC1'
        // docker command to start server container: 'sudo docker run --name ServerInstance --env 'ACCEPT_EULA=Y' --env 'SA_PASSWORD=Password!' --publish 1433:1433 -d mcr.microsoft.com/mssql/rhel/server:2019-RC1'
        // list containers: 'docker ps -a'
        // Connect to sql server instance: 'sqlcmd -s 0.0.0.0:1433 -U 'sa' -P 'Password!' '
        // ^ should connect you to container terminal, or you can use Azure Data Studio
        // username: sa password: Password! 

        // TO RUN:
        // -->> download .net Core sdk for Mac: 'https://dotnet.microsoft.com/download' <<--
        // ensure installed by running 'dotnet --version' from terminal
        // run local web server to host application using 'dotnet run' inside project directory
        // go to 'http://localhost:5000' to interact with application 

        private UpdateRecordsController _controller;
        public UpdateRecordsController Controller
        {  
            get { return _controller; }
            set { _controller = value; }
        }

        // This is the first method called by the runtime and it allows us to connect to the docker container
        public void ConfigureServices(IServiceCollection services)
        {
            // Setup connection string to database
            var connectionString = "Server=localhost;Database=Records;User Id=sa;Password=Password!";
            services.AddDbContext<FormDbContext>(db => db.UseSqlServer(connectionString));
            services.AddMvc(option => option.EnableEndpointRouting = false);
        }

        // This is the second method called by the runtime and will seed some data into a database
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, FormDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseStaticFiles();
            app.UseMvc();

            // Add some seed data to database
            bool seeded = context.CreateSeedData();
        }

    }
}
