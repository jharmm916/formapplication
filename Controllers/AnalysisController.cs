using System.Linq;
using Microsoft.AspNetCore.Mvc;
using formapplication.Entities;

namespace formapplication.Controllers {

    // Description: Updates context and manipulate records in database
    [Route("api/analyze")]
    public class AnalysisController : Controller 
    {
        private FormDbContext _context;

        public FormDbContext Context 
        { 
            get 
            {
                return _context;
            }
            set 
            {
                _context = value;
            }
        }

        public AnalysisController(FormDbContext context) 
        {
            Context = context;
        }

        // Assets - Liabilities
        [Route("Networth")]
        public string Networth() {
            var assets = Context.Records.Where(entry => entry.Type == "Asset").ToList();
            var liabilities = Context.Records.Where(entry => entry.Type == "Liability").ToList();
            var assetsTotal = 0.0;
            foreach (var asset in assets) {
                assetsTotal += asset.Balance;
            }
            var liabilitiesTotal = 0.0;
            foreach (var item in liabilities) {
                liabilitiesTotal += item.Balance;
            }
            return (assetsTotal - liabilitiesTotal).ToString();
        }

        // Assets total 
        [Route("AssetsTotal")]
        public string AssetsTotal() {
            var assets = Context.Records.Where(entry => entry.Type == "Asset").ToList();
            var assetsTotal = 0.0;
            foreach (var asset in assets) {
                assetsTotal += asset.Balance;
            }
            return assetsTotal.ToString();
        }

        // Liabilities total
        [Route("LiabilitiesTotal")]
        public string LiabilitiesTotal() {
            var liabilities = Context.Records.Where(entry => entry.Type == "Liability").ToList();
            var liabilitiesTotal = 0.0;
            foreach (var item in liabilities) {
                liabilitiesTotal += item.Balance;
            }
            return liabilitiesTotal.ToString();
        }

    }
}