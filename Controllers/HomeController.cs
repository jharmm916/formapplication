using Microsoft.AspNetCore.Mvc;

namespace formapplication.Controllers 
{
    [Route("/")]
    public class HomeController : Controller
    {
        public IActionResult Index() {
            return View();
        }
    }
}