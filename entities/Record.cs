using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace formapplication.Entities
{
     public class Record
     {
          [Key]
          [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
          public int Id { get; set; }

          [Required]
          [MaxLength(30)]
          public string Type { get; set; }

          [Required]
          [MaxLength(100)]
          public string Name { get; set; }

          [Required]
          public double Balance { get; set; }
          
     }
}